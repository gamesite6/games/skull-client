const path = require("path");

const production = process.env.NODE_ENV === "production";

module.exports = {
  entry: ["./src/index.js"],
  mode: production ? "production" : "development",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "component.js",
    library: "component",
    libraryTarget: "umd"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: "elm-webpack-loader",
          options: {
            optimize: production
          }
        }
      }
    ]
  }
};
