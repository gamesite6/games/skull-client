module Gen.Skull exposing (..)

-- The following module comes from bartavelle/json-helpers

import Dict exposing (Dict)
import Json.Decode
import Json.Encode exposing (Value)
import Json.Helpers exposing (..)
import Set exposing (Set)


type Action
    = Add Disc
    | Bid Int
    | Pass
    | FlipAll
    | Flip PlayerId
    | DiscardOwn Disc
    | Discard
    | SuccessOk


jsonDecAction : Json.Decode.Decoder Action
jsonDecAction =
    let
        jsonDecDictAction =
            Dict.fromList
                [ ( "Add", Json.Decode.lazy (\_ -> Json.Decode.map Add jsonDecDisc) )
                , ( "Bid", Json.Decode.lazy (\_ -> Json.Decode.map Bid Json.Decode.int) )
                , ( "Pass", Json.Decode.lazy (\_ -> Json.Decode.succeed Pass) )
                , ( "FlipAll", Json.Decode.lazy (\_ -> Json.Decode.succeed FlipAll) )
                , ( "Flip", Json.Decode.lazy (\_ -> Json.Decode.map Flip jsonDecPlayerId) )
                , ( "DiscardOwn", Json.Decode.lazy (\_ -> Json.Decode.map DiscardOwn jsonDecDisc) )
                , ( "Discard", Json.Decode.lazy (\_ -> Json.Decode.succeed Discard) )
                , ( "SuccessOk", Json.Decode.lazy (\_ -> Json.Decode.succeed SuccessOk) )
                ]
    in
    decodeSumObjectWithSingleField "Action" jsonDecDictAction


jsonEncAction : Action -> Value
jsonEncAction val =
    let
        keyval v =
            case v of
                Add v1 ->
                    ( "Add", encodeValue (jsonEncDisc v1) )

                Bid v1 ->
                    ( "Bid", encodeValue (Json.Encode.int v1) )

                Pass ->
                    ( "Pass", encodeValue (Json.Encode.list identity []) )

                FlipAll ->
                    ( "FlipAll", encodeValue (Json.Encode.list identity []) )

                Flip v1 ->
                    ( "Flip", encodeValue (jsonEncPlayerId v1) )

                DiscardOwn v1 ->
                    ( "DiscardOwn", encodeValue (jsonEncDisc v1) )

                Discard ->
                    ( "Discard", encodeValue (Json.Encode.list identity []) )

                SuccessOk ->
                    ( "SuccessOk", encodeValue (Json.Encode.list identity []) )
    in
    encodeSumObjectWithSingleField keyval val


type alias State =
    { players : List Player
    , phase : Phase
    , speech : List ( PlayerId, Speech )
    }


jsonDecState : Json.Decode.Decoder State
jsonDecState =
    Json.Decode.succeed (\pplayers pphase pspeech -> { players = pplayers, phase = pphase, speech = pspeech })
        |> required "players" (Json.Decode.list jsonDecPlayer)
        |> required "phase" jsonDecPhase
        |> required "speech" (Json.Decode.list (Json.Decode.map2 tuple2 (Json.Decode.index 0 jsonDecPlayerId) (Json.Decode.index 1 jsonDecSpeech)))


jsonEncState : State -> Value
jsonEncState val =
    Json.Encode.object
        [ ( "players", Json.Encode.list jsonEncPlayer val.players )
        , ( "phase", jsonEncPhase val.phase )
        , ( "speech", Json.Encode.list (\( t1, t2 ) -> Json.Encode.list identity [ jsonEncPlayerId t1, jsonEncSpeech t2 ]) val.speech )
        ]


type Speech
    = RaisedBid Int
    | Passed


jsonDecSpeech : Json.Decode.Decoder Speech
jsonDecSpeech =
    let
        jsonDecDictSpeech =
            Dict.fromList
                [ ( "RaisedBid", Json.Decode.lazy (\_ -> Json.Decode.map RaisedBid Json.Decode.int) )
                , ( "Passed", Json.Decode.lazy (\_ -> Json.Decode.succeed Passed) )
                ]
    in
    decodeSumObjectWithSingleField "Speech" jsonDecDictSpeech


jsonEncSpeech : Speech -> Value
jsonEncSpeech val =
    let
        keyval v =
            case v of
                RaisedBid v1 ->
                    ( "RaisedBid", encodeValue (Json.Encode.int v1) )

                Passed ->
                    ( "Passed", encodeValue (Json.Encode.list identity []) )
    in
    encodeSumObjectWithSingleField keyval val


type alias Settings =
    Set Int


jsonDecSettings : Json.Decode.Decoder Settings
jsonDecSettings =
    decodeSet Json.Decode.int


jsonEncSettings : Settings -> Value
jsonEncSettings val =
    encodeSet Json.Encode.int val


type alias Player =
    { id : PlayerId
    , hand : Discs
    , faceDown : List Disc
    , faceUp : List Disc
    , score : Int
    }


jsonDecPlayer : Json.Decode.Decoder Player
jsonDecPlayer =
    Json.Decode.succeed (\pid phand pfaceDown pfaceUp pscore -> { id = pid, hand = phand, faceDown = pfaceDown, faceUp = pfaceUp, score = pscore })
        |> required "id" jsonDecPlayerId
        |> required "hand" jsonDecDiscs
        |> required "faceDown" (Json.Decode.list jsonDecDisc)
        |> required "faceUp" (Json.Decode.list jsonDecDisc)
        |> required "score" Json.Decode.int


jsonEncPlayer : Player -> Value
jsonEncPlayer val =
    Json.Encode.object
        [ ( "id", jsonEncPlayerId val.id )
        , ( "hand", jsonEncDiscs val.hand )
        , ( "faceDown", Json.Encode.list jsonEncDisc val.faceDown )
        , ( "faceUp", Json.Encode.list jsonEncDisc val.faceUp )
        , ( "score", Json.Encode.int val.score )
        ]


type Phase
    = Prepare PreparePhase
    | AddOrChallenge PlayerId
    | BidOrPass BidOrPassPhase
    | FlipOwn FlipOwnPhase
    | Attempt AttemptPhase
    | DiscardSelf PlayerId
    | DiscardTarget DiscardPhase
    | Success PlayerId
    | GameComplete


jsonDecPhase : Json.Decode.Decoder Phase
jsonDecPhase =
    let
        jsonDecDictPhase =
            Dict.fromList
                [ ( "Prepare", Json.Decode.lazy (\_ -> Json.Decode.map Prepare jsonDecPreparePhase) )
                , ( "AddOrChallenge", Json.Decode.lazy (\_ -> Json.Decode.map AddOrChallenge jsonDecPlayerId) )
                , ( "BidOrPass", Json.Decode.lazy (\_ -> Json.Decode.map BidOrPass jsonDecBidOrPassPhase) )
                , ( "FlipOwn", Json.Decode.lazy (\_ -> Json.Decode.map FlipOwn jsonDecFlipOwnPhase) )
                , ( "Attempt", Json.Decode.lazy (\_ -> Json.Decode.map Attempt jsonDecAttemptPhase) )
                , ( "DiscardSelf", Json.Decode.lazy (\_ -> Json.Decode.map DiscardSelf jsonDecPlayerId) )
                , ( "DiscardTarget", Json.Decode.lazy (\_ -> Json.Decode.map DiscardTarget jsonDecDiscardPhase) )
                , ( "Success", Json.Decode.lazy (\_ -> Json.Decode.map Success jsonDecPlayerId) )
                , ( "GameComplete", Json.Decode.lazy (\_ -> Json.Decode.succeed GameComplete) )
                ]
    in
    decodeSumObjectWithSingleField "Phase" jsonDecDictPhase


jsonEncPhase : Phase -> Value
jsonEncPhase val =
    let
        keyval v =
            case v of
                Prepare v1 ->
                    ( "Prepare", encodeValue (jsonEncPreparePhase v1) )

                AddOrChallenge v1 ->
                    ( "AddOrChallenge", encodeValue (jsonEncPlayerId v1) )

                BidOrPass v1 ->
                    ( "BidOrPass", encodeValue (jsonEncBidOrPassPhase v1) )

                FlipOwn v1 ->
                    ( "FlipOwn", encodeValue (jsonEncFlipOwnPhase v1) )

                Attempt v1 ->
                    ( "Attempt", encodeValue (jsonEncAttemptPhase v1) )

                DiscardSelf v1 ->
                    ( "DiscardSelf", encodeValue (jsonEncPlayerId v1) )

                DiscardTarget v1 ->
                    ( "DiscardTarget", encodeValue (jsonEncDiscardPhase v1) )

                Success v1 ->
                    ( "Success", encodeValue (jsonEncPlayerId v1) )

                GameComplete ->
                    ( "GameComplete", encodeValue (Json.Encode.list identity []) )
    in
    encodeSumObjectWithSingleField keyval val


type alias PlayerId =
    Int


jsonDecPlayerId : Json.Decode.Decoder PlayerId
jsonDecPlayerId =
    Json.Decode.int


jsonEncPlayerId : PlayerId -> Value
jsonEncPlayerId val =
    Json.Encode.int val


type Disc
    = Rose
    | Skull


jsonDecDisc : Json.Decode.Decoder Disc
jsonDecDisc =
    let
        jsonDecDictDisc =
            Dict.fromList [ ( "Rose", Rose ), ( "Skull", Skull ) ]
    in
    decodeSumUnaries "Disc" jsonDecDictDisc


jsonEncDisc : Disc -> Value
jsonEncDisc val =
    case val of
        Rose ->
            Json.Encode.string "Rose"

        Skull ->
            Json.Encode.string "Skull"


type alias Discs =
    { roses : Int
    , skulls : Int
    }


jsonDecDiscs : Json.Decode.Decoder Discs
jsonDecDiscs =
    Json.Decode.succeed (\proses pskulls -> { roses = proses, skulls = pskulls })
        |> required "roses" Json.Decode.int
        |> required "skulls" Json.Decode.int


jsonEncDiscs : Discs -> Value
jsonEncDiscs val =
    Json.Encode.object
        [ ( "roses", Json.Encode.int val.roses )
        , ( "skulls", Json.Encode.int val.skulls )
        ]


type alias PreparePhase =
    { active : List PlayerId
    , startingPlayerId : PlayerId
    }


jsonDecPreparePhase : Json.Decode.Decoder PreparePhase
jsonDecPreparePhase =
    Json.Decode.succeed (\pactive pstartingPlayerId -> { active = pactive, startingPlayerId = pstartingPlayerId })
        |> required "active" (Json.Decode.list jsonDecPlayerId)
        |> required "startingPlayerId" jsonDecPlayerId


jsonEncPreparePhase : PreparePhase -> Value
jsonEncPreparePhase val =
    Json.Encode.object
        [ ( "active", Json.Encode.list jsonEncPlayerId val.active )
        , ( "startingPlayerId", jsonEncPlayerId val.startingPlayerId )
        ]


type alias BidOrPassPhase =
    { highestBid : Int
    , highestBidder : PlayerId
    , playerId : PlayerId
    , passed : List PlayerId
    }


jsonDecBidOrPassPhase : Json.Decode.Decoder BidOrPassPhase
jsonDecBidOrPassPhase =
    Json.Decode.succeed (\phighestBid phighestBidder pplayerId ppassed -> { highestBid = phighestBid, highestBidder = phighestBidder, playerId = pplayerId, passed = ppassed })
        |> required "highestBid" Json.Decode.int
        |> required "highestBidder" jsonDecPlayerId
        |> required "playerId" jsonDecPlayerId
        |> required "passed" (Json.Decode.list jsonDecPlayerId)


jsonEncBidOrPassPhase : BidOrPassPhase -> Value
jsonEncBidOrPassPhase val =
    Json.Encode.object
        [ ( "highestBid", Json.Encode.int val.highestBid )
        , ( "highestBidder", jsonEncPlayerId val.highestBidder )
        , ( "playerId", jsonEncPlayerId val.playerId )
        , ( "passed", Json.Encode.list jsonEncPlayerId val.passed )
        ]


type alias FlipOwnPhase =
    { playerId : PlayerId
    , bid : Int
    }


jsonDecFlipOwnPhase : Json.Decode.Decoder FlipOwnPhase
jsonDecFlipOwnPhase =
    Json.Decode.succeed (\pplayerId pbid -> { playerId = pplayerId, bid = pbid })
        |> required "playerId" jsonDecPlayerId
        |> required "bid" Json.Decode.int


jsonEncFlipOwnPhase : FlipOwnPhase -> Value
jsonEncFlipOwnPhase val =
    Json.Encode.object
        [ ( "playerId", jsonEncPlayerId val.playerId )
        , ( "bid", Json.Encode.int val.bid )
        ]


type alias AttemptPhase =
    { playerId : PlayerId
    , goal : Int
    }


jsonDecAttemptPhase : Json.Decode.Decoder AttemptPhase
jsonDecAttemptPhase =
    Json.Decode.succeed (\pplayerId pgoal -> { playerId = pplayerId, goal = pgoal })
        |> required "playerId" jsonDecPlayerId
        |> required "goal" Json.Decode.int


jsonEncAttemptPhase : AttemptPhase -> Value
jsonEncAttemptPhase val =
    Json.Encode.object
        [ ( "playerId", jsonEncPlayerId val.playerId )
        , ( "goal", Json.Encode.int val.goal )
        ]


type alias DiscardPhase =
    { playerId : PlayerId
    , targetId : PlayerId
    }


jsonDecDiscardPhase : Json.Decode.Decoder DiscardPhase
jsonDecDiscardPhase =
    Json.Decode.succeed (\pplayerId ptargetId -> { playerId = pplayerId, targetId = ptargetId })
        |> required "playerId" jsonDecPlayerId
        |> required "targetId" jsonDecPlayerId


jsonEncDiscardPhase : DiscardPhase -> Value
jsonEncDiscardPhase val =
    Json.Encode.object
        [ ( "playerId", jsonEncPlayerId val.playerId )
        , ( "targetId", jsonEncPlayerId val.targetId )
        ]
