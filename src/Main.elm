port module Main exposing (main)

import Browser
import Dict exposing (Dict)
import Gen.Skull exposing (..)
import Html exposing (Html, br, button, code, div, form, input, node, p, span, strong, text)
import Html.Attributes as Attr
import Html.Events exposing (onClick)
import Json.Decode as D
import Json.Encode as E
import Json.Helpers as H
import List.Extra exposing (splitWhen)


main : Program E.Value Model Msg
main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


type alias Model =
    Result String
        { props : Props
        , inputs : Inputs
        }


type alias Inputs =
    { bid : String
    }


initialInputs : Inputs
initialInputs =
    { bid = "" }


type alias Props =
    { userId : Maybe UserId
    , state : State
    , settings : Settings
    }


jsonDecProps : D.Decoder Props
jsonDecProps =
    D.map3 Props
        (D.field "userId" (D.maybe D.int))
        (D.field "state" jsonDecState)
        (D.field "settings" jsonDecSettings)


type alias UserId =
    Int


type alias User =
    { id : UserId
    , name : String
    }


userDecoder : D.Decoder User
userDecoder =
    D.map2 User
        (D.field "id" D.int)
        (D.field "name" D.string)


init : E.Value -> ( Model, Cmd Msg )
init initialStateJson =
    case D.decodeValue jsonDecProps initialStateJson of
        Ok props ->
            ( Ok
                { props = props
                , inputs = initialInputs
                }
            , Cmd.none
            )

        Err err ->
            ( Err (D.errorToString err), Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    setProps PropsChanged


port onAction : E.Value -> Cmd msg


port setProps : (E.Value -> msg) -> Sub msg


type Msg
    = PerformAction Action
    | PropsChanged E.Value
    | ChangeBidInput String
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg modelRes =
    case ( msg, modelRes ) of
        ( PerformAction action, Ok model ) ->
            ( Ok model, onAction (jsonEncAction action) )

        ( PropsChanged propsJson, _ ) ->
            case D.decodeValue jsonDecProps propsJson of
                Ok nextProps ->
                    let
                        inputs =
                            case modelRes of
                                Ok model ->
                                    model.inputs

                                Err _ ->
                                    initialInputs
                    in
                    ( Ok
                        { props = nextProps
                        , inputs = inputs
                        }
                    , Cmd.none
                    )

                Err err ->
                    ( Err (D.errorToString err), Cmd.none )

        ( ChangeBidInput value, Ok model ) ->
            let
                inputs =
                    model.inputs
            in
            ( Ok { model | inputs = { inputs | bid = value } }, Cmd.none )

        ( NoOp, _ ) ->
            ( modelRes, Cmd.none )

        ( _, Err err ) ->
            ( Err err, Cmd.none )


view : Model -> Html Msg
view model =
    case model of
        Err err ->
            code [] [ text err ]

        Ok { props, inputs } ->
            let
                { userId, settings, state } =
                    props

                _ =
                    settings

                orderedPlayers =
                    orderPlayers userId state.players
            in
            div
                [ Attr.style "position" "relative"
                , Attr.style "height" "100%"
                , Attr.style "display" "flex"
                , Attr.style "justify-content" "center"
                , Attr.style "align-items" "center"
                , Attr.style "flex-direction" "column"
                ]
                (List.concat
                    [ orderedPlayers
                        |> List.indexedMap (playerView userId inputs state)
                    , case state.phase of
                        Prepare _ ->
                            []

                        AddOrChallenge _ ->
                            []

                        BidOrPass { highestBid, highestBidder } ->
                            [ div [] [ text "Highest bid" ]
                            , div [] [ text (String.concat [ String.fromInt highestBid, " discs, by ", String.fromInt highestBidder ]) ]
                            ]

                        FlipOwn { bid } ->
                            [ div [] [ text (String.concat [ String.fromInt (flippedRoses state.players), "\u{2009}/\u{2009}", String.fromInt bid ]) ] ]

                        Attempt { goal } ->
                            [ div [] [ text (String.concat [ String.fromInt (flippedRoses state.players), "\u{2009}/\u{2009}", String.fromInt goal ]) ] ]

                        Success playerId ->
                            [ div [] [ text "Success" ]
                            , div []
                                [ text (String.concat [ String.fromInt playerId, " scores a point!" ]) ]
                            , p []
                                (case List.Extra.find (\p -> p.id == playerId) state.players of
                                    Just player ->
                                        [ text (String.concat [ String.fromInt player.score, " ⟶ ", String.fromInt (player.score + 1) ]) ]

                                    Nothing ->
                                        []
                                )
                            , if userId == Just playerId then
                                button [ Attr.type_ "button", onClick (PerformAction SuccessOk) ] [ text "Ok" ]

                              else
                                text ""
                            ]

                        DiscardTarget { playerId, targetId } ->
                            [ div [] [ text "Failure" ]
                            , div [] [ text (String.concat [ String.fromInt playerId, " must discard one of ", String.fromInt targetId, "'s discs." ]) ]
                            ]

                        DiscardSelf playerId ->
                            [ div [] [ text "Failure" ]
                            , div [] [ text (String.concat [ String.fromInt playerId, " must discard one of their own discs." ]) ]
                            ]

                        GameComplete ->
                            [ text "Game is over" ]
                    ]
                )


flippedRoses : List Player -> Int
flippedRoses =
    List.sum << List.map (List.Extra.count ((==) Rose) << .faceUp)


isActive : Phase -> UserId -> Bool
isActive phase userId =
    case phase of
        Prepare { active } ->
            List.member userId active

        AddOrChallenge playerId ->
            playerId == userId

        BidOrPass { playerId } ->
            playerId == userId

        FlipOwn { playerId } ->
            playerId == userId

        Attempt { playerId } ->
            playerId == userId

        DiscardSelf playerId ->
            playerId == userId

        DiscardTarget { playerId } ->
            playerId == userId

        Success playerId ->
            playerId == userId

        GameComplete ->
            False


canAddDisc : Phase -> UserId -> Bool
canAddDisc phase playerId =
    case phase of
        Prepare { active } ->
            List.member playerId active

        AddOrChallenge activeId ->
            activeId == playerId

        _ ->
            False


canBid : Phase -> UserId -> Bool
canBid phase playerId =
    case phase of
        AddOrChallenge activeId ->
            playerId == activeId

        BidOrPass ({ highestBidder, passed } as bidOrPass) ->
            (bidOrPass.playerId == playerId)
                && (playerId /= highestBidder)
                && not (List.member playerId passed)

        _ ->
            False


canPass : Phase -> UserId -> Bool
canPass phase playerId =
    case phase of
        BidOrPass ({ highestBidder, passed } as bidOrPass) ->
            (bidOrPass.playerId == playerId)
                && (playerId /= highestBidder)
                && not (List.member playerId passed)

        _ ->
            False


canDiscardOwn : Phase -> UserId -> Bool
canDiscardOwn phase playerId =
    case phase of
        DiscardSelf activePlayerId ->
            playerId == activePlayerId

        _ ->
            False


type DiscFace
    = FaceUp
    | FaceDown


discView : DiscFace -> Disc -> Html Msg
discView discFace disc =
    case ( discFace, disc ) of
        ( FaceUp, Rose ) ->
            text "🏵"

        ( FaceUp, Skull ) ->
            text "💀"

        ( FaceDown, _ ) ->
            text "⚫"


type alias Index =
    Int


playerView : Maybe UserId -> Inputs -> State -> Index -> Player -> Html Msg
playerView maybeUserId inputs state i player =
    let
        isUser =
            maybeUserId == Just player.id

        isActivePlayer =
            isActive state.phase player.id

        playerCanAddDisc =
            canAddDisc state.phase player.id

        playerCanBid : Bool
        playerCanBid =
            canBid state.phase player.id

        rotation : Float
        rotation =
            toFloat i * 360.0 / toFloat (List.length state.players)
    in
    node "gs6-player-box"
        [ Attr.attribute "userid" (String.fromInt player.id)
        , Attr.attribute "thinking"
            (if isActivePlayer then
                "true"

             else
                ""
            )
        , Attr.style "position" "absolute"
        , Attr.style "left" "50%"
        , Attr.style "top" "50%"
        , Attr.style "transform"
            (String.concat
                [ "translateX(-50%) translateY(-50%) rotate(", String.fromFloat rotation, "deg) translateY(15em) rotate(", String.fromFloat -rotation, "deg)" ]
            )
        ]
        [ div [] [ text "Score: ", text (String.fromInt player.score) ]
        , if isUser && playerCanBid then
            let
                highestBid =
                    case state.phase of
                        BidOrPass p ->
                            p.highestBid

                        _ ->
                            0

                totalPlayedDiscs =
                    state.players
                        |> List.map (\p -> List.length p.faceDown)
                        |> List.sum
            in
            form
                [ Html.Events.onSubmit
                    (case String.toInt inputs.bid of
                        Just bid ->
                            PerformAction (Bid bid)

                        Nothing ->
                            NoOp
                    )
                ]
                [ input
                    [ Attr.type_ "number"
                    , Attr.value inputs.bid
                    , Attr.min (String.fromInt (highestBid + 1))
                    , Attr.max (String.fromInt totalPlayedDiscs)
                    , Html.Events.onInput ChangeBidInput
                    ]
                    []
                , text " "
                , button [] [ text "Bid" ]
                , if canPass state.phase player.id then
                    span []
                        [ text " "
                        , button [ Attr.type_ "button", onClick (PerformAction Pass) ] [ text "Pass" ]
                        ]

                  else
                    text ""
                ]

          else
            text ""
        , div []
            [ text "Hand "
            , span []
                (List.concat
                    [ List.range 1 player.hand.skulls
                        |> List.map
                            (\_ ->
                                span []
                                    [ case ( isUser, state.phase ) of
                                        ( True, DiscardSelf _ ) ->
                                            button
                                                [ onClick (PerformAction (DiscardOwn Skull))
                                                ]
                                                [ discView FaceUp Skull ]

                                        ( True, _ ) ->
                                            button
                                                [ Attr.disabled (not playerCanAddDisc)
                                                , onClick (PerformAction (Add Skull))
                                                ]
                                                [ discView FaceUp Skull ]

                                        _ ->
                                            discView FaceDown Skull
                                    ]
                            )
                    , List.range 1 player.hand.roses
                        |> List.map
                            (\_ ->
                                span []
                                    [ case ( isUser, state.phase ) of
                                        ( True, DiscardSelf _ ) ->
                                            button
                                                [ onClick (PerformAction (DiscardOwn Rose))
                                                ]
                                                [ discView FaceUp Rose ]

                                        ( True, _ ) ->
                                            button
                                                [ Attr.disabled (not playerCanAddDisc)
                                                , onClick (PerformAction (Add Rose))
                                                ]
                                                [ discView FaceUp Rose ]

                                        _ ->
                                            discView FaceDown Rose
                                    ]
                            )
                    ]
                )
            , case ( state.phase, maybeUserId ) of
                ( DiscardTarget { targetId, playerId }, Just userId ) ->
                    if playerId == userId && player.id == targetId then
                        button [ Attr.type_ "button", onClick (PerformAction Discard) ] [ text "Discard" ]

                    else
                        text ""

                _ ->
                    text ""
            ]
        , div []
            [ text "Played "
            , span []
                (player.faceDown
                    |> List.map (\disc -> span [] [ discView FaceDown disc ])
                )
            , case state.phase of
                FlipOwn { playerId } ->
                    if isUser && isActivePlayer && playerId == player.id then
                        button [ Attr.type_ "button", onClick (PerformAction FlipAll) ] [ text "Flip all" ]

                    else
                        text ""

                Attempt { playerId } ->
                    if
                        (playerId /= player.id)
                            && (maybeUserId == Just playerId)
                            && not (List.isEmpty player.faceDown)
                    then
                        button [ Attr.type_ "button", onClick (PerformAction (Flip player.id)) ] [ text "Flip" ]

                    else
                        text ""

                _ ->
                    text ""
            ]
        , div []
            [ text "Flipped "
            , span []
                (player.faceUp
                    |> List.map
                        (\disc ->
                            span []
                                [ if isUser then
                                    button
                                        [ Attr.disabled (not (canDiscardOwn state.phase player.id))
                                        , onClick (PerformAction (DiscardOwn disc))
                                        ]
                                        [ discView FaceUp disc ]

                                  else
                                    discView FaceUp disc
                                ]
                        )
                )
            ]
        ]


orderPlayers : Maybe UserId -> List Player -> List Player
orderPlayers maybeUserId players =
    case maybeUserId of
        Nothing ->
            players

        Just userId ->
            case splitWhen (\plr -> plr.id == userId) players of
                Just ( before, user :: after ) ->
                    user :: (after ++ before)

                Just ( _, [] ) ->
                    players

                Nothing ->
                    players
