import main from "./Main.elm";

export default class SkullGame extends HTMLElement {
  constructor() {
    super();
  }

  set state(state) {
    this._state = state;
    this.setAttribute("state", JSON.stringify(state));
  }
  set settings(settings) {
    this._settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  connectedCallback() {
    let node = document.createElement("div");
    this.appendChild(node);
    this.app = main.Elm.Main.init({
      node,
      flags: {
        userId: parseInt(this.getAttribute("userid")),
        settings: this._settings,
        state: this._state,
      },
    });

    this.app.ports.onAction.subscribe((action) => {
      this.dispatchEvent(
        new CustomEvent("action", {
          detail: action,
          bubbles: false,
        })
      );
    });
  }
  static get observedAttributes() {
    return ["userid", "settings", "state"];
  }
  attributeChangedCallback(name, oldValue, newValue) {
    if (this.app && oldValue !== newValue) {
      this.app.ports.setProps.send({
        userId: parseInt(this.getAttribute("userid")),
        settings: this._settings,
        state: this._state,
      });
    }
  }
}
